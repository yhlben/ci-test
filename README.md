# ci-test

ci-test 库。

## 核心规范

- commit 规范。https://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html
- commitlint 校验是否按照规范提交。 https://commitlint.js.org/#/
- 版本号规范 https://semver.org/lang/zh-CN/
- changelog 规范 https://keepachangelog.com/zh-CN/0.3.0/

## 开发流程

- 编写代码
- 提交代码
- 合 master 时，执行版本号更新，npm run release

changelog 日志

https://gitlab.com/yhlben/ci-test/blob/master/CHANGELOG.md
