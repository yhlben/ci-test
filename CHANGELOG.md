# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/yhlben/ci-test/compare/v1.1.6...v1.2.0) (2019-11-11)


### Features

* xxx ([1e68f74](https://gitlab.com/yhlben/ci-test/commit/1e68f748aa79124a78a55551b9c1f7a3b94e689b))

### [1.1.6](https://gitlab.com/yhlben/ci-test/compare/v1.1.5...v1.1.6) (2019-11-11)

### [1.1.5](https://gitlab.com/yhlben/ci-test/compare/v1.1.4...v1.1.5) (2019-11-11)

### [1.1.4](https://gitlab.com/yhlben/ci-test/compare/v1.1.3...v1.1.4) (2019-11-11)

### [1.1.3](https://gitlab.com/yhlben/ci-test/compare/v1.1.2...v1.1.3) (2019-11-11)


### Bug Fixes

* 修复了 xxx bug ([0501359](https://gitlab.com/yhlben/ci-test/commit/050135984acd18634fd36dc40acb481809263556))

### [1.1.2](https://gitlab.com/yhlben/ci-test/compare/v1.1.1...v1.1.2) (2019-11-11)

### [1.1.1](https://gitlab.com/yhlben/ci-test/compare/v1.1.0...v1.1.1) (2019-11-11)

## [1.1.0](https://gitlab.com/yhlben/ci-test/compare/v1.0.4...v1.1.0) (2019-11-11)

### Features

- 精准营销展示模板 ([960b631](https://gitlab.com/yhlben/ci-test/commit/960b6314afc2f74c0f26c7c92317e50caeb6eec1))

### [1.0.4](https://gitlab.com/yhlben/ci-test/compare/v1.0.3...v1.0.4) (2019-11-11)

### [1.0.3](https://gitlab.com/yhlben/ci-test/compare/v1.0.2...v1.0.3) (2019-10-15)

### Bug Fixes

- change some code ([29088bb](https://gitlab.com/yhlben/ci-test/commit/29088bb6b5ed8d4f9255f21baac7e1a49b89acbd))

### [1.0.2](https://gitlab.com/yhlben/ci-test/compare/v1.0.1...v1.0.2) (2019-10-15)

### 1.0.1 (2019-10-15)
